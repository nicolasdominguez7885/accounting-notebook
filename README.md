# Accounting notebook
Application UI built using react, backend using spring

## Requirements for development
`Maven` and `npm`
## Build
Use `npm install` to build react application, and `mvn install` to build java

## Run
Make sure ports 5000 and 8080 are free.
Run from root folder:

Backend: 
`java -jar accounting-notebook-0.0.1-SNAPSHOT.jar`

Frontend:
`serve -s frontend-build`

## Suggested improvements if project were to conitune in future
Document API using swagger following OpenAPI standards

Multi user support.

Use a database.

Use Spring security for tokens.

Add Spring boot health check endpoints.

Add logging to a service which allows elastic search.

Frontend showing balance.

Order and filter transactions