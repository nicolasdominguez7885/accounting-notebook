import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import moment from 'moment';
import { CircularProgress } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    alignSelf: 'center',
    width: '50%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  error: {
    color: 'red',
  },
  credit: {
    color: 'green',
  },
  debit: {
    color: 'red',
  },
}));

const DATE_FORMAT = 'YYYY-MM-DD HH:mm';

export default function Transactions() {
  const [transactions, setTransactions] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');
  useEffect(() => {
    fetch('http://localhost:8080/transactions')
      .then(response => {
        //handle error codes
        return response.json()
      })
      .then(response => {
        setLoading(false);
        setTransactions(response)
      }).catch(e => {
        //TODO handle connection errors
        setLoading(false);
        setError(e.message);
      })
  }, []);

  const classes = useStyles();
  function formatDate(date) {
    return moment.utc(date).local().format(DATE_FORMAT);
  }

  return (
    <div className={classes.root}>
      {loading && <CircularProgress />}
      {error && <p className={classes.error}>
        {error}
      </p>}
      {!loading && ! error && transactions.length ? transactions.map((transaction, index) =>
        <Accordion key={index}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls={`${transaction.id}-content`}
            id={`${transaction.id}-header`}
            className={transaction.type === 'credit' ? classes.credit : classes.debit}
          >
            <Typography className={classes.heading}>{formatDate(transaction.effectiveDate)}</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Type: {transaction.type}
              <br/>
              Amount: {transaction.amount}
              <br/>
              Id: {transaction.id}
            </Typography>
          </AccordionDetails>
        </Accordion>
      ): <p>There are no transactions in the system yet.</p>}
    </div>
  );
}
