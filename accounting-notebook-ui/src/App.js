import { makeStyles } from '@material-ui/core';
import 'fontsource-roboto';
import Transactions from "./Transactions";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '2rem',
  },
  subtitle: {
    paddingLeft: '1rem',
    color: 'grey',
  }
}));

function App() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <h1>Accounting Notebok</h1>
      <p className={classes.subtitle}>For record keeping of my transactions</p>
      <Transactions transactions={[
        {id: "7df57ab6-7afe-48cc-b89b-6266933e7cbd", amount: 20, type: 'credit', effectiveDate: '2021-01-10T23:05:14.292Z'},
        {id: "6c279df4-3b14-4762-a177-a729d64c0030", amount: 10, type: 'debit', effectiveDate: '2021-01-10T23:15:14.292Z'},
        {id: "6c279df4-3b14-4762-a177-a729d64c0029", amount: 13, type: 'debit', effectiveDate: '2021-01-10T23:25:14.292Z'},
      ]}/>
    </div>
  );
}

export default App;
