package com.agileengine.nicolas.accountingnotebook.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="The id of the transaction request is invalid, must be of type UUID")
public class InvalidTransactionIdException extends RuntimeException{
    public InvalidTransactionIdException() {
        super("The id of the transaction request is invalid, must be of type UUID");
    }
}
