package com.agileengine.nicolas.accountingnotebook;

import com.agileengine.nicolas.accountingnotebook.exceptions.InvalidTransactionIdException;
import com.agileengine.nicolas.accountingnotebook.exceptions.InvalidTransactionType;
import com.agileengine.nicolas.accountingnotebook.exceptions.NegativeBalanceException;
import com.agileengine.nicolas.accountingnotebook.exceptions.TransactionNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

//TODO do not lock on read, only on writes, must use a lock to be acquired and that we can check if it is aquired instead of locking on full object
@Component
public class Account {
    @Value("${account.startingAmount}")
    private double balance;
    private final Map<String, Transaction> transactions = new LinkedHashMap<>();

    private void setBalance(double balance) {
        if (balance < 0.0) {
            throw new NegativeBalanceException();
        }
        this.balance = balance;
    }

    public synchronized double getBalance() {
        return balance;
    }

    public synchronized List<Transaction> getTransactionsList() {
        return new ArrayList<>(transactions.values());
    }

    public synchronized Transaction processTransaction(Transaction newTransaction) {
        //TODO check decimals respect allowed values
        //TODO use polymorphism
        switch (newTransaction.getType()) {
            case Transaction.CREDIT:
                credit(newTransaction);
                break;
            case Transaction.DEBIT:
                debit(newTransaction);
                break;
            default:
                throw new InvalidTransactionType();
        }
        newTransaction.setId(UUID.randomUUID().toString());
        newTransaction.setEffectiveDate(LocalDateTime.now().toString());
        transactions.put(newTransaction.getId(), newTransaction);
        return newTransaction;
    }

    public synchronized Transaction getTransaction(String transactionId) {
        try {
            UUID.fromString(transactionId);
        } catch(Exception e) {
            throw new InvalidTransactionIdException();
        }
        if (!transactions.containsKey(transactionId)) {
            throw new TransactionNotFoundException();
        }
        return transactions.get(transactionId);
    }

    private Transaction credit(Transaction newTransaction) {
        setBalance(balance + newTransaction.getAmount());
        return newTransaction;
    }

    private Transaction debit(Transaction newTransaction) {
        setBalance(balance - newTransaction.getAmount());
        return newTransaction;
    }
}
