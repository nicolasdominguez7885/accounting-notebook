package com.agileengine.nicolas.accountingnotebook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SpringBootApplication
@RestController
public class AccountingNotebookApplication {
	@Autowired
	private Account account;

	public static void main(String[] args) {
		SpringApplication.run(AccountingNotebookApplication.class, args);
	}

	@GetMapping("/balance")
	public double getBalance() {
		return account.getBalance();
	}

	@GetMapping("/transactions")
	public List<Transaction> getTransactions() {
		return account.getTransactionsList();
	}

	@PostMapping("/transactions")
	@ResponseBody
	public Transaction postTransaction(@RequestBody Transaction newTransaction) {
		return account.processTransaction(newTransaction);
	}

	@GetMapping("/transactions/{id}")
	public Transaction getTransaction(@PathVariable("id") String transactionId) {
		return account.getTransaction(transactionId);
	}
}
