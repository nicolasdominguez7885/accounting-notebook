package com.agileengine.nicolas.accountingnotebook.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="The type of transaction requests is invalid")
public class InvalidTransactionType extends RuntimeException{
    public InvalidTransactionType() {
        super("The type of transaction requests is invalid");
    }
}
