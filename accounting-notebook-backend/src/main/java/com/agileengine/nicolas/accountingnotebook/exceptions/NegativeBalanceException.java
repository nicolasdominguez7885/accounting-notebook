package com.agileengine.nicolas.accountingnotebook.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="Finalizing this transaction would leave you with a negative balance")
public class NegativeBalanceException extends RuntimeException{
    public NegativeBalanceException() {
        super("Finalizing this transaction would leave you with a negative balance");
    }
}
